async def application(scope, recv, send):
    print('scope: ', scope)
    print('receive: ', recv)
    print('send: ', send)

    event = await recv()
    print('event: ', event)

    await send({
        'type': 'http.response.start',
        'status': 200,
        'headers': [
            [b'content-type', b'text/plain'],
        ],
    })
    await send({
        'type': 'http.response.body',
        'body': b'Hello, world!',
    })
